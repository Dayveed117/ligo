module Loc = Simple_utils.Location
module Pos = Simple_utils.Pos
module Region = Simple_utils.Region
module LSet = Scopes.Types.LSet

let ( <@ ) = Simple_utils.Function.( <@ )
