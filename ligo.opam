name         : "ligo"
opam-version : "2.0"
maintainer   : "Galfour <contact@ligolang.org>"
authors      : [ "Gabriel Alfour" "Christian Rinderknecht"]
homepage     : "https://gitlab.com/ligolang/tezos"
bug-reports  : "https://gitlab.com/ligolang/tezos/issues"
synopsis     : "A high-level language which compiles to Michelson"
dev-repo     : "git+https://gitlab.com/ligolang/tezos.git"
license      : "MIT"

# If you change the dependencies, run `opam lock` in the root
depends: [
  # Jane Street Core
  "core" {>= "v0.15.0" & < "v0.16.0"}
  "core_kernel" { >= "v0.15.0" & < "v0.16.0"}
  # Tooling
  "js_of_ocaml"
  "js_of_ocaml-lwt"
  "odoc" { build }
  "ocamlfind" { build }
  "dune" { build & >= "3.6.1" }
  "alcotest" { with-test }
  # Pipelinye
  "ocamlgraph"
  "menhir" { = "20211128" }
  "coq" { build & >= "8.12" & < "8.14" }
  # I/O
  "bos"
  "tezos-clic"
  "qcheck"
  "terminal_size"
  "pprint"
  "yojson"
  "ocaml-recovery-parser" { = "0.2.4" }
  "semver"
  "uri"
  "tls"
  "decompress"
  "tar"
  "tar-unix"
  "lambda-term"
  "parse-argv"
  # Tezos libs
  "tezos-base"
  "tezos-crypto"
  "tezos-micheline"
  # PPXs
  "js_of_ocaml-ppx"
  "ppx_deriving"
  "ppx_deriving_yojson"
  "ppx_yojson_conv"
  "ppx_expect"
  "ppx_import"
  "ppx_inline_test"
  # Analytics
  "asetmap" {= "0.8.1"}
  "prometheus" {>= "1.2"}
  # work around tezos' failure to constrain
  "lwt" {= "5.6.1"}
  "bisect_ppx" {>= "2.3"}
  "irmin"
  "cmdliner" {= "1.1.0"}
  "ocaml-compiler-libs"
  "simple-diff"
  # work around upstream in-place update
  "ocaml-migrate-parsetree" { = "2.3.0" }
  # dependencies of vendored dependencies
  "bls12-381"
  "alcotest-lwt"
  "qcheck-alcotest"
  "irmin-pack"
  "pure-splitmix"
  "resto-cohttp-self-serving-client"
  "tezos-rust-libs"
  "crunch"
  "class_group_vdf"
  "tezos-plonk" { >= "1.0.1" & < "2.0.0" }
  # dev dependencies (not labels as { dev } since this feature doesn't work 
  # currently https://github.com/ocaml/opam/issues/5177).
  # Also sticking to 0.21.0 for ocamlformat since ocamlformat-rpc doesn't have a
  # 0.24.1 release
  "ocamlformat" { = "0.21.0" }
  "ocamlformat-rpc" { = "0.21.0" }
  "ocaml-lsp-server" { >= "1.14.0" }
  # dependencies for ligo lsp (OCaml implementation)
  "lsp" { = "1.14.2" }
  "linol" { = "0.4" }
  "linol-lwt" { = "0.4" }
  "aches-lwt"
  # Networking
  "conduit" {= "6.1.0"}
]
build: [
  [ "dune" "build" "-p" name "-j" jobs ]
]
pin-depends: [
  [ "tezos-base.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-clic.dev" "./vendors/tezos-ligo/opam" ]
  [ "tc-015.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-client-base.dev" "./vendors/tezos-ligo/opam" ]
  [ "tz-b-c-ux.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-client-commands.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-context.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-crypto.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-error-monad.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-event-logging.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-hacl.dev" "./vendors/tezos-ligo/opam"]
  [ "tz-lwtres-std.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-micheline.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-mockup.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-mockup-commands.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-mockup-proxy.dev" "./vendors/tezos-ligo/opam" ]
  [ "tz-mock-reg.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-p2p.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-p2p-services.dev" "./vendors/tezos-ligo/opam" ]
  [ "tp-016.dev" "./vendors/tezos-ligo/opam" ]
  [ "octez-protocol-compiler.dev" "./vendors/tezos-ligo/opam" ]
  [ "tpe.dev" "./vendors/tezos-ligo/opam" ]
  [ "tp-plugin-015.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-proxy.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-rpc.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-rpc-http.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-rpc-http-client.dev" "./vendors/tezos-ligo/opam" ]
  [ "tz-rpc-h-c-ux.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-sapling.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-shell-services.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-signer-backends.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-signer-services.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-stdlib.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-stdlib-unix.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-test-helpers.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-version.dev" "./vendors/tezos-ligo/opam" ]
  [ "tezos-workers.dev" "./vendors/tezos-ligo/opam" ]
  [ "ocaml-recovery-parser.0.2.4" "git+https://github.com/serokell/ocaml-recovery-parser.git#0.2.4" ]
  [ "linol.0.4" "git+https://github.com/c-cube/linol.git#439534e0c5b7a3fbf93ba05fae7d171426153763" ]
  [ "linol-lwt.0.4" "git+https://github.com/c-cube/linol.git#439534e0c5b7a3fbf93ba05fae7d171426153763" ]
]
